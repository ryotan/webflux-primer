package me.ryotan.primer.spring.webflux.receiver;

import me.ryotan.primer.spring.webflux.pipeline.EventPipelineManager;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("receive")
public class EventReceiveController {
    private final EventPipelineManager<SomeEvent> eventPipelineManager;

    public EventReceiveController(EventPipelineManager<SomeEvent> eventPipelineManager) {
        this.eventPipelineManager = eventPipelineManager;
    }

    @PostMapping("some-event")
    public HttpStatus receiveSomeEvent(@RequestBody SomeEvent body) {
        eventPipelineManager.get(body.getId()).put(body);
        return HttpStatus.OK;
    }
}
