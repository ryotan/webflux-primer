package me.ryotan.primer.spring.webflux.pipeline;

import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;

@Component
public class EventPipelineManager<T> {
    private ConcurrentHashMap<String, EventPipeline<T>> pipelines = new ConcurrentHashMap<>();

    public EventPipeline<T> get(String id) {
        return pipelines.computeIfAbsent(id, i -> new EventPipeline<>());
    }
}
