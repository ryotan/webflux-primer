package me.ryotan.primer.spring.webflux.emitter;

import me.ryotan.primer.spring.webflux.pipeline.EventPipelineManager;
import me.ryotan.primer.spring.webflux.receiver.SomeEvent;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("source")
public class EventEmitController {
    private final EventPipelineManager<SomeEvent> eventPipelineManager;

    public EventEmitController(EventPipelineManager<SomeEvent> eventPipelineManager) {
        this.eventPipelineManager = eventPipelineManager;
    }

    @GetMapping("some-event")
    public Flux<ServerSentEvent<SomeEvent>> sourceSomeEvent(@RequestParam String id) {
        return Flux.create(sink -> eventPipelineManager.get(id).addListener((e) -> sink.next(ServerSentEvent.builder(e).build())));
    }
}
