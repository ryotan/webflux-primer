package me.ryotan.primer.spring.webflux;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWebfluxPrimerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringWebfluxPrimerApplication.class, args);
    }
}
