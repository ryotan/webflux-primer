package me.ryotan.primer.spring.webflux.pipeline;

import java.util.ArrayList;
import java.util.List;

public class EventPipeline<T> {
    private List<EventPipelineListener<T>> listeners = new ArrayList<>();

    public void addListener(EventPipelineListener<T> listener) {
        this.listeners.add(listener);
    }

    public void put(T event) {
        this.listeners.parallelStream().forEach(l -> l.onEvent(event));
    }
}
