package me.ryotan.primer.spring.webflux.pipeline;

public interface EventPipelineListener<T> {
    void onEvent(T event);
}
